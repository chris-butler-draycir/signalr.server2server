﻿using Draycir.SDC.Integration.Api;

namespace signalR.server2server.OnPremiseService
{
    public class AchiveDocumentService : IService
    {
        private const string Sage200ApplicationName = "Sage 200";
        private const string DemoCompanyId = "1";
        private const string DemoCompanyDisplayName = "Demo Company";
        private const string HostName = "localhost";
        private const int PortNumber = 34251;
        private const string DeveloperLicenceKey = "<Draycir Root TEST><WX2AW06A6HMFQSBPNL13AQPLK7QQMQVM1BT0NT3WVWKRZBQDAZ>";

        private SdcServerEndpoint _sdcServerEndpoint;

        public AchiveDocumentService()
        {
            _sdcServerEndpoint = new SdcServerEndpoint(HostName, PortNumber)
            {
                LicenceKey = DeveloperLicenceKey
            };
        }

        public void Start()
        {
            
        }

        public void Stop()
        {
            
        }
    }
}
