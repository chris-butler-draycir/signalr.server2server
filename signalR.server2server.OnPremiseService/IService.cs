﻿namespace signalR.server2server.OnPremiseService
{
    public interface IService
    {
        void Start();

        void Stop();
    }
}